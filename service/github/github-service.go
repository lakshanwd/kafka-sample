package github

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// GitHubServiceBuilder represents the builder for GitHub service
type GitHubServiceBuilder interface {
	// WithClient sets the client
	WithClient(client *http.Client) GitHubServiceBuilder

	// Build builds the GitHub service
	Build() GitHubSvc
}

type gsBuilder struct {
	client *http.Client
}

// NewGitHubSvcBuilder creates a new GitHub service builder
func NewGitHubSvcBuilder() GitHubServiceBuilder {
	return &gsBuilder{client: http.DefaultClient}
}

// WithClient implements GitHubServiceBuilder.WithClient
func (gsb *gsBuilder) WithClient(client *http.Client) GitHubServiceBuilder {
	gsb.client = client
	return gsb
}

// Build implements GitHubServiceBuilder.Build
func (gsb *gsBuilder) Build() GitHubSvc {
	return &gitHubService{client: gsb.client}
}

// GitHubSvc represents the service for GitHub
type GitHubSvc interface {
	// FetchUserInfo fetches user info from github
	FetchUserInfo(ctx context.Context, username string) (map[string]interface{}, error)
}

type gitHubService struct {
	client *http.Client
}

// GHUser represents a github user
type GHUser struct {
	ID           int    `json:"id"`
	Login        string `json:"login"`
	FollowersUrl string `json:"followers_url"`
	ReposUrl     string `json:"repos_url"`
}

// Repo represents a github repo
type Repo struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Owner    GHUser `json:"owner"`
	FullName string `json:"full_name"`
}

// FetchUserInfo implements GitHubSvc.FetchUserInfo
func (gs *gitHubService) FetchUserInfo(ctx context.Context, username string) (map[string]interface{}, error) {

	// read user info from github
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("https://api.github.com/users/%s", username), nil)
	if err != nil {
		log.Printf("error creating request: %v", err)
		return nil, err
	}
	userResp, err := gs.client.Do(req)
	if err != nil {
		log.Println("error fetching user info", err)
		return nil, err
	}
	defer userResp.Body.Close()
	var u GHUser
	if err := json.NewDecoder(userResp.Body).Decode(&u); err != nil {
		log.Println("error decoding user info", err)
		return nil, err
	}

	// read followers from github
	req, err = http.NewRequestWithContext(ctx, http.MethodGet, u.FollowersUrl, nil)
	if err != nil {
		log.Printf("error creating followers request: %v", err)
		return nil, err
	}
	followersResp, err := gs.client.Do(req)
	if err != nil {
		log.Println("error fetching followers", err)
		return nil, err
	}
	defer followersResp.Body.Close()
	var followers []GHUser
	if err := json.NewDecoder(followersResp.Body).Decode(&followers); err != nil {
		log.Println("error decoding followers", err)
		return nil, err
	}

	// read repos from github
	req, err = http.NewRequestWithContext(ctx, http.MethodGet, u.ReposUrl, nil)
	if err != nil {
		log.Printf("error creating repos request: %v", err)
		return nil, err
	}
	reposResp, err := gs.client.Do(req)
	if err != nil {
		log.Println("error fetching repos", err)
		return nil, err
	}
	defer reposResp.Body.Close()
	var repos []Repo
	if err := json.NewDecoder(reposResp.Body).Decode(&repos); err != nil {
		log.Println("error decoding repos", err)
		return nil, err
	}

	return map[string]interface{}{
		"id":        u.ID,
		"username":  u.Login,
		"followers": followers,
		"repos":     repos,
	}, nil
}

package kafka

import (
	"encoding/json"
	"log"

	"github.com/Shopify/sarama"
)

// KafkaSvc is the interface for the Kafka service
type KafkaSvc interface {
	// Consume consumes the messages from the Kafka topic
	Consume(callback Callback)

	// Produce produces the messages to the Kafka topic
	Produce(payload interface{}) (partition int32, offset int64, err error)

	// Close closes the Kafka connection
	Close()
}

// Callback is the function that will be called when a message is received
type Callback func(msg *sarama.ConsumerMessage, err *sarama.ConsumerError)

// NewKafkaSvc creates a new Kafka service
func NewKafkaSvc(brokers []string, topic string) (KafkaSvc, error) {
	// connect kafka consumer
	worker, err := connectConsumer(brokers)
	if err != nil {
		log.Printf("failed to start consumer: %s", err)
		return nil, err
	}

	// connect kafka partition consumer
	partitionConsumer, err := worker.ConsumePartition(topic, 0, sarama.OffsetOldest)
	if err != nil {
		log.Printf("Could not start consumer for partition %d: %s", 0, err)
		return nil, err
	}

	// connect kafka producer
	producer, err := connectProducer(brokers)
	if err != nil {
		log.Printf("failed to start producer: %s", err)
		return nil, err
	}

	return &kafkaInstance{brokers: brokers, topic: topic, consumer: worker, producer: producer, partitionConsumer: partitionConsumer}, nil
}

type kafkaInstance struct {
	brokers           []string
	topic             string
	consumer          sarama.Consumer
	producer          sarama.SyncProducer
	partitionConsumer sarama.PartitionConsumer
}

// Consume implements KafkaSvc
func (ki *kafkaInstance) Consume(callback Callback) {
	for {
		select {
		case err := <-ki.partitionConsumer.Errors():
			callback(nil, err)
		case msg := <-ki.partitionConsumer.Messages():
			callback(msg, nil)
		}
	}
}

// Close implements KafkaSvc
func (ki *kafkaInstance) Close() {
	ki.partitionConsumer.Close()
	ki.consumer.Close()
	ki.producer.Close()
}

// Produce implements KafkaSvc
func (ki *kafkaInstance) Produce(payload interface{}) (partition int32, offset int64, err error) {
	// marshal payload
	bytes, err := json.Marshal(payload)
	if err != nil {
		log.Printf("failed to marshal payload: %s", err)
		return 0, 0, err
	}

	// construct message
	msg := &sarama.ProducerMessage{
		Topic: ki.topic,
		Value: sarama.StringEncoder(bytes),
	}

	// produce message
	return ki.producer.SendMessage(msg)
}

func connectConsumer(brokersUrl []string) (sarama.Consumer, error) {
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true

	// Create new consumer
	conn, err := sarama.NewConsumer(brokersUrl, config)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func connectProducer(brokersUrl []string) (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5

	conn, err := sarama.NewSyncProducer(brokersUrl, config)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

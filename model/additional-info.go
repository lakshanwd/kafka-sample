package model

type AdditionalInfo struct {
	Data struct {
		Email      string `json:"email"`
		FirstName  string `json:"first_name"`
		LastName   string `json:"last_name"`
		TimeZoneID string `json:"time_zone_id"`
	} `json:"data"`
}

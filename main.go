package main

import (
	"flag"
	"log"
	"my-budget/app"
)

var configPath = flag.String("config", "./config.yml", "path to config file")

func main() {
	// parse flags
	flag.Parse()

	// initialize application
	app, err := app.NewApplication(*configPath)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("initialized application")

	// releasing resources
	defer app.StopServices()

	// start services
	app.StartServices()
}

package app

import (
	"fmt"
	"log"
	"my-budget/config"
	"my-budget/controller"
	"my-budget/manager"
	"my-budget/service/github"
	"my-budget/service/kafka"
	"net/http"
	"os"

	"github.com/Shopify/sarama"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gopkg.in/yaml.v3"
)

// Application represents the application
type Application struct {
	githubSvc github.GitHubSvc
	kafkaSvc  kafka.KafkaSvc
}

// NewApplication creates a new application instance
func NewApplication(configPath string) (*Application, error) {
	file, err := os.Open(configPath)
	if err != nil {
		log.Printf("error opening config file: %v", err)
		return nil, err
	}

	var localCfg config.GeneralConfig
	yaml.NewDecoder(file).Decode(&localCfg)
	config.Settings = localCfg

	// init github service
	githubSvc := github.NewGitHubSvcBuilder().Build()

	// init kafka service
	kafkaSvc, err := kafka.NewKafkaSvc(localCfg.KafkaBrokers, localCfg.Topic)
	if err != nil {
		log.Printf("error initializing kafka service: %v", err)
		return nil, err
	}

	return &Application{
		githubSvc: githubSvc,
		kafkaSvc:  kafkaSvc,
	}, nil
}

// registerRoutes registers all routes
func (app *Application) registerRoutes() chi.Router {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	userMgr := manager.NewUserManager(app.githubSvc, app.kafkaSvc)
	eventCtrl := controller.NewEventController(userMgr)
	r.Post("/produce/{user_id}", eventCtrl.ProduceUser)
	r.Put("/users/{user_id}", eventCtrl.UpdateUser)

	return r
}

// StartServices starts all services
func (app *Application) StartServices() {
	// register kafka consumers
	app.kafkaSvc.Consume(func(msg *sarama.ConsumerMessage, err *sarama.ConsumerError) {
		if err != nil {
			log.Printf("error consuming message: %v", err)
			return
		}
		log.Printf("message consumed: %s", msg.Value)
	})

	// register routes
	r := app.registerRoutes()
	log.Println("initialized router")

	// start server
	err := http.ListenAndServe(fmt.Sprintf(":%d", config.Settings.Port), r)
	if err != nil {
		log.Panic(err)
	}
}

// StopServices stops all services
func (app *Application) StopServices() {
	app.kafkaSvc.Close()
}

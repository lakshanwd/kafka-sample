package config

type GeneralConfig struct {
	Port         int      `yaml:"port"`
	KafkaBrokers []string `yaml:"kafka_brokers"`
	Topic        string   `yaml:"topic"`
}

var Settings GeneralConfig

package controller

import (
	"my-budget/manager"
	"my-budget/model"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type EventController interface {
	ProduceUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
}

type eventController struct {
	userManager manager.UserManager
}

func NewEventController(usrMgr manager.UserManager) EventController {
	return &eventController{userManager: usrMgr}
}

func (ec *eventController) ProduceUser(w http.ResponseWriter, r *http.Request) {
	userName := chi.URLParam(r, "user_id")
	err := ec.userManager.ProduceUser(r.Context(), userName)
	if err != nil {
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, render.M{"error": err.Error(), "success": false})
		return
	}
	render.Status(r, http.StatusCreated)
	render.JSON(w, r, render.M{"success": true})
}

func (ec *eventController) UpdateUser(w http.ResponseWriter, r *http.Request) {
	// parsing body
	var reqPayload model.AdditionalInfo
	err := render.DecodeJSON(r.Body, &reqPayload)
	if err != nil {
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, render.M{"error": err.Error(), "success": false})
		return
	}

	err = ec.userManager.UpdateUser(r.Context(), reqPayload)
	if err != nil {
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, render.M{"error": err.Error(), "success": false})
		return
	}
	render.Status(r, http.StatusAccepted)
	render.JSON(w, r, render.M{"success": true})
}

package manager

import (
	"context"
	"log"
	"my-budget/event"
	"my-budget/model"
	"my-budget/service/github"
	"my-budget/service/kafka"
)

type UserManager interface {
	// ProduceUser fetches user info from github and sends it to kafka
	ProduceUser(ctx context.Context, userName string) error

	// UpdateUser updates user info in kafka
	UpdateUser(ctx context.Context, data model.AdditionalInfo) error
}

type userManager struct {
	github github.GitHubSvc
	kafka  kafka.KafkaSvc
}

func NewUserManager(github github.GitHubSvc, kafka kafka.KafkaSvc) UserManager {
	return &userManager{
		github: github,
		kafka:  kafka,
	}
}

// ProduceUser implements UserManager.ProduceUser
func (mgr *userManager) ProduceUser(ctx context.Context, userName string) error {
	// fetch user info from github
	userinfo, err := mgr.github.FetchUserInfo(ctx, userName)
	if err != nil {
		log.Printf("Failed to fetch user info from github: %v", err)
		return err
	}

	// send user info to kafka
	partition, offset, err := mgr.kafka.Produce(event.Payload{
		ID:        userinfo["id"].(int),
		Username:  userinfo["login"].(string),
		Followers: userinfo["followers"].([]github.GHUser),
		Repos:     userinfo["repos"].([]github.Repo),
		Email:     userinfo["email"].(string),
	})
	if err != nil {
		log.Printf("Failed to produce user info to kafka: %v", err)
		return err
	}
	log.Printf("Produced user info to kafka: partition=%d, offset=%d", partition, offset)
	return nil
}

// UpdateUser implements UserManager.UpdateUser
func (mgr *userManager) UpdateUser(ctx context.Context, wrapper model.AdditionalInfo) error {
	// send user info to kafka
	partition, offset, err := mgr.kafka.Produce(event.Payload{
		Email:      wrapper.Data.Email,
		FirstName:  wrapper.Data.FirstName,
		LastName:   wrapper.Data.LastName,
		TimeZoneID: wrapper.Data.TimeZoneID,
	})
	if err != nil {
		log.Printf("Failed to produce user info to kafka: %v", err)
		return err
	}
	log.Printf("Produced user info to kafka: partition=%d, offset=%d", partition, offset)
	return nil
}

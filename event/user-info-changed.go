package event

import (
	"my-budget/service/github"

	"github.com/google/uuid"
)

type UserInfoChanged struct {
	Meta    Meta    `json:"meta"`
	Payload Payload `json:"payload"`
}

type Meta struct {
	Type      string    `json:"type"`
	EventID   uuid.UUID `json:"event_id"`
	CreatedAt int64     `json:"created_at"`
	TraceID   uuid.UUID `json:"trace_id"`
	ServiceID string    `json:"service_id"`
}

type Payload struct {
	ID         int             `json:"id"`
	Username   string          `json:"username"`
	Followers  []github.GHUser `json:"followers"`
	Repos      []github.Repo   `json:"repos"`
	Email      string          `json:"email"`
	FirstName  string          `json:"first_name"`
	LastName   string          `json:"last_name"`
	TimeZoneID string          `json:"time_zone_id"`
}
